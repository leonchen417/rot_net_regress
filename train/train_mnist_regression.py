from __future__ import print_function

import os
import sys
import tensorflow as tf


#from tf.keras.layers import  Input
#from tf.keras.layers import  MaxPooling2D
#from tf.keras.models import Model

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from utils import angle_error_regression, RotNetDataGenerator, binarize_images


# we don't need the labels indicating the digit value, so we only load the images
(X_train, _), (X_test, _) = tf.keras.datasets.mnist.load_data()

model_name = 'rotnet_mnist_regression'

# number of convolutional filters to use
nb_filters = 64
# size of pooling area for max pooling
pool_size = (2, 2)
# convolution kernel size
kernel_size = (3, 3)

nb_train_samples, img_rows, img_cols = X_train.shape
img_channels = 1
input_shape = (img_rows, img_cols, img_channels)
nb_test_samples = X_test.shape[0]
print('Input shape:', input_shape)
print(nb_train_samples, 'train samples')
print(nb_test_samples, 'test samples')

# model definition
input = tf.keras.layers.Input(shape=(img_rows, img_cols, img_channels))
x = tf.keras.layers.Conv2D(nb_filters, kernel_size, activation='relu')(input)
x = tf.keras.layers.Conv2D(nb_filters, kernel_size, activation='relu')(x)
x = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(x)
x = tf.keras.layers.Dropout(0.25)(x)
x = tf.keras.layers.Flatten()(x)
x = tf.keras.layers.Dense(128, activation='relu')(x)
x = tf.keras.layers.Dropout(0.25)(x)
x = tf.keras.layers.Dense(1, activation='sigmoid')(x)

model = tf.keras.models.Model(inputs=input, outputs=x)

model.summary()


# model compilation
model.compile(loss=angle_error_regression,
              optimizer='adam')

# training parameters
batch_size = 128
nb_epoch = 50

output_folder = 'models'
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

# callbacks
checkpointer = tf.keras.callbacks.ModelCheckpoint(
    filepath=os.path.join(output_folder, model_name + '.hdf5'),
    save_best_only=True
)
early_stopping = tf.keras.callbacks.EarlyStopping(patience=2)
tensorboard = tf.keras.callbacks.TensorBoard()

# training loop
model.fit_generator(
    RotNetDataGenerator(
        X_train,
        batch_size=batch_size,
        one_hot=False,
        preprocess_func=binarize_images,
        shuffle=True
    ),
    steps_per_epoch=nb_train_samples / batch_size,
    epochs=nb_epoch,
    validation_data=RotNetDataGenerator(
        X_test,
        one_hot=False,
        preprocess_func=binarize_images,
        batch_size=batch_size
    ),
    validation_steps=nb_test_samples / batch_size,
    verbose=1,
    callbacks=[checkpointer, early_stopping, tensorboard]
)
#https://www.tensorflow.org/api_docs/python/tf/keras/models/save_model
#https://machinelearningmastery.com/save-load-keras-deep-learning-models/